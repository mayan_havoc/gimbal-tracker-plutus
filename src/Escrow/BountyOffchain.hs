{-# LANGUAGE TypeApplications #-}
{-#LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeOperators #-}

module Escrow.BountyOffchain where 

import           Ledger.Constraints       as Constraints
import Plutus.Contract as Contract
import Data.Text (Text)
import qualified Data.Map                  as Map
import qualified PlutusTx
import Ledger 
import Prelude as Pr
import              Ledger.Value        as Value
import           Data.Aeson                (ToJSON, FromJSON)
import           GHC.Generics              (Generic)
import           Schema                    (ToSchema)
import qualified Plutus.V1.Ledger.Ada     as Ada (lovelaceValueOf)
import           Control.Monad        hiding (fmap)

import Escrow.BountyTreasury as BountyTreasury
import Escrow.BountyTypes as BountyTypes
import qualified Escrow.BountyEscrow as BountyEscrow

type BountySchema =  Endpoint "lockFundsTreasury" LockTreasuryParams
                     .\/ Endpoint "contributeBounty" UpdateTreasuryParams
                     .\/ Endpoint "cancelBounty" UpdateTreasuryParams
                     .\/ Endpoint "updateBounty" UpdateTreasuryParams
                     .\/ Endpoint "distributeBounty" UpdateTreasuryParams


data LockTreasuryParams = LockTreasuryParams
    { ltpBountyCs :: CurrencySymbol 
    , ltpBountyTn :: TokenName
    , ltpBountyAmount :: Integer
    , ltpGimbalsAm :: Integer
    , ltpAdaAm :: Integer
    , ltpAuthCs :: CurrencySymbol
    } deriving (Pr.Eq, Pr.Ord, Show, Generic, ToJSON, FromJSON, ToSchema)


-- cardano-cli transaction build \
    --alonzo-era \
    --tx-in $TXIN1 \
    --tx-in $TXIN2 \
    --tx-out $TREASURY+"99000000 + 400 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
    --tx-out-datum-embed-file treasury-withdrawal-datum.json \
    --tx-out $SENDER+"2000000 + 11200 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
    --change-address $SENDER \
    --protocol-params-file protocol.json \
    --out-file tx.raw \
    --testnet-magic 1097911063


lockFundsTreasury :: LockTreasuryParams -> Contract w BountySchema Text ()
lockFundsTreasury params = do
    pkh <- Contract.ownPaymentPubKeyHash
    ownUtxos <- utxosAt (pubKeyHashAddress pkh Nothing) 
    let dat = BountyTypes.WithdrawalDatum (ltpBountyAmount params) (unPaymentPubKeyHash pkh)
        bCs = ltpBountyCs params
        bTn = ltpBountyTn params
        bountyVal = Value.singleton bCs bTn (ltpGimbalsAm params)
        bountyParam = BountyTypes.BountyParam bCs bTn (ltpAuthCs params) (unPaymentPubKeyHash pkh)
        treasuryParam = BountyTypes.TreasuryParam (ltpAuthCs params) (BountyEscrow.escrowValidatorHash bountyParam) bCs bTn (unPaymentPubKeyHash pkh)
        lookups = Constraints.unspentOutputs ownUtxos <>
                  Constraints.typedValidatorLookups (BountyTreasury.typedValidator treasuryParam)                
        tx      = Constraints.mustPayToTheScript dat (bountyVal <> Ada.lovelaceValueOf (ltpAdaAm params))
    ledgerTx <- submitTxConstraintsWith @BountyTypes.TreasuryTypes lookups tx 
    void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
    Contract.logInfo @Pr.String "lock transaction confirmed"

data UpdateTreasuryParams = UpdateTreasuryParams
    { utpBountyCs :: CurrencySymbol 
    , utpBountyTn :: TokenName
    , utpBountyAmount :: Integer
    , utpAuthCs :: CurrencySymbol
    , utpAuthTn :: TokenName
    , utpIssuerPkh :: PaymentPubKeyHash 
    , utpContributorPkh :: PaymentPubKeyHash 
    , utpExpirationTime :: POSIXTime 
    , utpLovelaceAmount :: Integer
    } deriving (Pr.Eq, Pr.Ord, Show, Generic, ToJSON, FromJSON, ToSchema)


contributeBounty :: UpdateTreasuryParams -> Contract w BountySchema Text ()
contributeBounty params = do
    pkh <- Contract.ownPaymentPubKeyHash
    -- get our own utxos we later need for sending our AuthToken 
    ownUtxos <- utxosAt (pubKeyHashAddress pkh Nothing)
    let bCs = utpBountyCs params
        bTn = utpBountyTn params
        issuerPpkh = unPaymentPubKeyHash $ utpIssuerPkh params
        contributorPpkh = unPaymentPubKeyHash $ utpContributorPkh params
        -- BountyParam to build escrow instance 
        bountyParam = BountyTypes.BountyParam bCs bTn (utpAuthCs params) issuerPpkh 
        -- TreasuryParam we want to use 
        treasuryParam = BountyTypes.TreasuryParam (utpAuthCs params) (BountyEscrow.escrowValidatorHash bountyParam) bCs bTn issuerPpkh 
        -- Treasury contract address we want to use  
        scrAddrT = scriptAddress $ BountyTreasury.validator treasuryParam
        bAm = utpBountyAmount params
    -- search utxo we want to consume 
    (oref, o, d) <- findUtxoTreasury bCs bTn scrAddrT bAm
    let authVal = Value.singleton (utpAuthCs params) (utpAuthTn params) 1
        --bountyDetails as  Redeemer 
        bountyDetails = BountyTypes.BountyDetails issuerPpkh contributorPpkh (utpLovelaceAmount params) bAm (utpExpirationTime params)
        rTreasury = Redeemer $ PlutusTx.toBuiltinData bountyDetails
        -- treasury value - value to escrow = new treasury value 
        utxoVal = _ciTxOutValue o
        adaValNew = valueOf utxoVal "" "" - utpLovelaceAmount params
        bValNew = valueOf utxoVal bCs bTn - bAm
        valNew = Value.singleton bCs bTn bValNew <> Ada.lovelaceValueOf adaValNew 
        -- value we send to the escrow contract 
        toBountyVal = Ada.lovelaceValueOf (utpLovelaceAmount params) <> Value.singleton bCs bTn bAm
        -- create escrow datum with same parameters we used for bountyDetails 
        dBounty = Datum $ PlutusTx.toBuiltinData $ BountyEscrowDatum issuerPpkh contributorPpkh (utpLovelaceAmount params) bAm (utpExpirationTime params)
        -- we don't have to build the WithDrawalDatum, plutus does it for us but we have to provide typedValidator, uspentOutputs and validator  
        lookups = Constraints.typedValidatorLookups (BountyTreasury.typedValidator treasuryParam) <> 
                  Constraints.unspentOutputs (Map.singleton oref o) <> 
                  Constraints.unspentOutputs ownUtxos <>
                  Constraints.otherScript (BountyTreasury.validator treasuryParam)
        -- cardano-cli transaction build 
        tx = Constraints.mustPayToOtherScript (BountyTreasury.treasuryValidatorHash treasuryParam) (Datum $ PlutusTx.toBuiltinData d) valNew <>
             Constraints.mustPayToOtherScript (BountyEscrow.escrowValidatorHash bountyParam) dBounty (toBountyVal <> authVal) <> 
             Constraints.mustSpendScriptOutput oref rTreasury 
    -- cardano-cli transaction sign / cardano-cli transaction submit 
    ledgerTx <- submitTxConstraintsWith @BountyTypes.TreasuryTypes lookups tx
    -- wait for phase 2 validation 
    void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
    Contract.logInfo @Pr.String "contribute transaction confirmed"

cancelBounty :: UpdateTreasuryParams -> Contract w BountySchema Text () 
cancelBounty params = do 
    pkh <- Contract.ownPaymentPubKeyHash 
    let bountyParam = BountyTypes.BountyParam (utpBountyCs params) (utpBountyTn params) (utpAuthCs params) (unPaymentPubKeyHash pkh)
        scrAddrE = scriptAddress $ BountyEscrow.validator bountyParam
    (oref, o, d) <- findUtxoEscrow (utpAuthCs params) (utpAuthTn params) scrAddrE
    let rEscrow = Redeemer $ PlutusTx.toBuiltinData Cancel 
        lookupsEscrow = Constraints.typedValidatorLookups (BountyEscrow.typedValidator bountyParam) <> 
                         Constraints.unspentOutputs (Map.singleton oref o) <> 
                         Constraints.otherScript (BountyEscrow.validator bountyParam)
        txEscrow = Constraints.mustSpendScriptOutput oref rEscrow <> Constraints.mustPayToPubKey pkh (_ciTxOutValue o) <> Constraints.mustValidateIn (from $ bedExpirationTime d) 
    Contract.logInfo @Pr.String $ "outval: " <> show (_ciTxOutValue o) 
    ledgerTx <- submitTxConstraintsWith lookupsEscrow txEscrow
    void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
    Contract.logInfo @Pr.String "cancelBounty transaction confirmed"


updateBounty :: UpdateTreasuryParams -> Contract w BountySchema Text () 
updateBounty params = do 
    pkh <- Contract.ownPaymentPubKeyHash 
    let bountyParam = BountyTypes.BountyParam (utpBountyCs params) (utpBountyTn params) (utpAuthCs params) (unPaymentPubKeyHash pkh)
        scrAddrE = scriptAddress $ BountyEscrow.validator bountyParam
    (oref, o, d) <- findUtxoEscrow (utpAuthCs params) (utpAuthTn params) scrAddrE
    let rEscrow = Redeemer $ PlutusTx.toBuiltinData Update
        dEscrow = d {bedTokenAmount = 55}
        lookupsEscrow = Constraints.typedValidatorLookups (BountyEscrow.typedValidator bountyParam) <> 
                         Constraints.unspentOutputs (Map.singleton oref o) <> 
                         Constraints.otherScript (BountyEscrow.validator bountyParam)
        txEscrow = Constraints.mustSpendScriptOutput oref rEscrow <> Constraints.mustPayToTheScript dEscrow (_ciTxOutValue o)
    Contract.logInfo @Pr.String $ "outval: " <> show (_ciTxOutValue o) 
    ledgerTx <- submitTxConstraintsWith lookupsEscrow txEscrow
    void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
    Contract.logInfo @Pr.String "updateBounty transaction confirmed"
    Contract.logInfo @Pr.String $ "Datum: " <> show dEscrow

distributeBounty :: UpdateTreasuryParams -> Contract w BountySchema Text () 
distributeBounty params = do
    pkh <- Contract.ownPaymentPubKeyHash 
    ownUtxos <- utxosAt $ pubKeyHashAddress pkh Nothing
    let bountyParam = BountyTypes.BountyParam (utpBountyCs params) (utpBountyTn params) (utpAuthCs params) (unPaymentPubKeyHash pkh)
        scrAddrE = scriptAddress $ BountyEscrow.validator bountyParam
    (oref, o, d) <- findUtxoEscrow (utpAuthCs params) (utpAuthTn params) scrAddrE
    let rEscrow = Redeemer $ PlutusTx.toBuiltinData Distribute
        valToCon = Value.singleton (utpBountyCs params) (utpBountyTn params) (bedTokenAmount d) <> Value.singleton "" "" (bedLovelaceAmount d) 
        lookupsEscrow = Constraints.typedValidatorLookups (BountyEscrow.typedValidator bountyParam) <> 
                         Constraints.unspentOutputs (Map.singleton oref o) <> 
                         Constraints.otherScript (BountyEscrow.validator bountyParam) <>
                         Constraints.unspentOutputs ownUtxos
        txEscrow = Constraints.mustSpendScriptOutput oref rEscrow <> Constraints.mustPayToPubKey (PaymentPubKeyHash (bedContributorPkh d)) valToCon <> Constraints.mustBeSignedBy pkh
    Contract.logInfo @Pr.String $ "outval: " <> show (bedTokenAmount d)
    ledgerTx <- submitTxConstraintsWith lookupsEscrow txEscrow
    void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
    Contract.logInfo @Pr.String "distribute transaction confirmed"



findUtxoTreasury :: CurrencySymbol -> TokenName -> Address -> Integer -> Contract w s Text (TxOutRef, ChainIndexTxOut, WithdrawalDatum)
findUtxoTreasury cs tn scr i = do
    -- get all utxos 
    utxos <- utxosAt scr
    -- we need one with enough bountyTokens 
    let xs = [ (oref, o)
             | (oref, o) <- Map.toList utxos
             , Value.valueOf (_ciTxOutValue o) cs tn >= i
             ]
    case xs of
        -- Datum can be Nothing  
        [(oref, o)] -> case _ciTxOutDatum o of
            Left _          -> throwError "Treasury datum missing"
            -- correct datum type? 
            Right (Datum e) -> case PlutusTx.fromBuiltinData e of
                Nothing -> throwError "Treasury datum has wrong type"
                -- make datum type WithdrawalDatum 
                Just d@WithdrawalDatum {}
                    | True -> return (oref, o, d)
                    | otherwise                                           -> throwError "Treasury utxo token missmatch"
        _           -> throwError "Treasury utxo not found"

findUtxoEscrow :: CurrencySymbol -> TokenName -> Address -> Contract w s Text (TxOutRef, ChainIndexTxOut, BountyEscrowDatum)
findUtxoEscrow cs tn scr = do
    utxos <- utxosAt scr
    let xs = [ (oref, o)
             | (oref, o) <- Map.toList utxos
             , Value.valueOf (_ciTxOutValue o) cs tn >= 1
             ]
    case xs of
        [(oref, o)] -> case _ciTxOutDatum o of
            Left _          -> throwError "Escrow datum missing"
            Right (Datum e) -> case PlutusTx.fromBuiltinData e of
                Nothing -> throwError "Escrow datum has wrong type"
                Just d@BountyEscrowDatum {}
                    | True -> return (oref, o, d)
                    | otherwise                                           -> throwError "Escrow utxo token missmatch"
        _           -> throwError "Escrow utxo not found"


endpoints :: Contract () BountySchema Text ()
endpoints = forever
          $ handleError logError
          $ awaitPromise 
          $ lockFundsTreasury' `select` contributeBounty' `select` cancelBounty' `select` updateBounty' `select` distributeBounty'
  where
    lockFundsTreasury'  = endpoint @"lockFundsTreasury" $ \params -> lockFundsTreasury params 
    contributeBounty'   = endpoint @"contributeBounty" $ \params -> contributeBounty params 
    cancelBounty'       = endpoint @"cancelBounty" $ \params -> cancelBounty params 
    updateBounty'       = endpoint @"updateBounty" $ \params -> updateBounty params 
    distributeBounty'   = endpoint @"distributeBounty"    $ \params -> distributeBounty params 

