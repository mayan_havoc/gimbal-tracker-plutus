{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}


module Escrow.BountyTypes
    ( TreasuryParam (..)
    , WithdrawalDatum (..)
    , BountyDetails (..)
    , BountyAction (..)
    , BountyParam (..)
    , BountyEscrowDatum (..)
    , TreasuryTypes
    , EscrowTypes
    , escrowDatum
    ) where

import           Data.Aeson                (ToJSON, FromJSON)
import           GHC.Generics              (Generic)
import           Schema                    (ToSchema)
import              Ledger              hiding (singleton)
import Ledger.Typed.Scripts
import qualified    PlutusTx
import              PlutusTx.Prelude    hiding (Semigroup (..), unless)
import              Prelude             (Show (..))
import qualified Prelude                   as Pr


-- Escrow.BountyTreasury
-- mkValidator :: TreasuryParam -> WithdrawalDatum -> BountyDetails -> ScriptContext -> Bool

data TreasuryParam = TreasuryParam
    { atSymbol          :: !CurrencySymbol
    , bountyContract    :: !ValidatorHash
    , bSymbol           :: !CurrencySymbol
    , bName             :: !TokenName
    , tIssuerPkh        :: !PubKeyHash
    } deriving (Pr.Eq, Pr.Ord, Show, Generic, ToJSON, FromJSON, ToSchema)

PlutusTx.makeLift ''TreasuryParam

data WithdrawalDatum = WithdrawalDatum
  { bountyCount     :: !Integer
  , treasuryKey     :: !PubKeyHash
  } deriving (Pr.Eq, Pr.Ord, Show, Generic, ToJSON, FromJSON, ToSchema)


PlutusTx.unstableMakeIsData ''WithdrawalDatum

data TreasuryTypes

instance ValidatorTypes TreasuryTypes where
    type DatumType TreasuryTypes = WithdrawalDatum
    type RedeemerType TreasuryTypes = BountyDetails



-- Use Redeemer to submit a "request" to Treasury for x lovelace and y gimbals
-- Validator will check that datum is being created to match request

data BountyDetails = BountyDetails
  {
    issuerPkh       :: !PubKeyHash
  , contributorPkh  :: !PubKeyHash
  , lovelaceAmount      :: !Integer
  , tokenAmount         :: !Integer
  , expirationTime      :: !POSIXTime
  } deriving (Pr.Eq, Pr.Ord, Show, Generic, ToJSON, FromJSON, ToSchema)


PlutusTx.unstableMakeIsData ''BountyDetails
PlutusTx.makeLift ''BountyDetails

-- BountyEscrow
-- INLINABLE to use On Chain
{-# INLINABLE escrowDatum #-}
escrowDatum :: TxOut -> (DatumHash -> Maybe Datum) -> Maybe BountyEscrowDatum
escrowDatum o f = do
    dh <- txOutDatum o
    Datum d <- f dh
    PlutusTx.fromBuiltinData d


data BountyEscrowDatum = BountyEscrowDatum
  { bedIssuerPkh       :: !PubKeyHash
  , bedContributorPkh  :: !PubKeyHash
  , bedLovelaceAmount      :: !Integer
  , bedTokenAmount         :: !Integer
  , bedExpirationTime      :: !POSIXTime
  } deriving (Pr.Eq, Pr.Ord, Show, Generic, ToJSON, FromJSON, ToSchema)


PlutusTx.unstableMakeIsData ''BountyEscrowDatum

-- V2: Add Issuer's PKH as treasury Param
-- The treasuryIssuerPkh is included here in order to create a unique Bounty Escrow contract and address
data BountyParam = BountyParam
    { bountyTokenPolicyId     :: !CurrencySymbol
    , bountyTokenName         :: !TokenName
    , accessTokenPolicyId     :: !CurrencySymbol
    , treasuryIssuerPkh       :: !PubKeyHash
    } deriving (Pr.Eq, Pr.Ord, Show, Generic, ToJSON, FromJSON, ToSchema)


PlutusTx.makeLift ''BountyParam

-- Usage: a bounty is created when a Contributor commits to it
-- Before that it is ready + "listed" off-chain (and here's a cool chance to create a Haskell API server)
-- v0 implements a "good faith" escrow, where funds are held, but still controlled by the bounty issuer
-- an upcoming iteration can use a token to represent each bounty, and that token can be used for unlocking
-- (which raises the question, who holds that token, and when?)
-- In any case, for now, the following actions will do:

data BountyAction = Cancel | Update | Distribute
  deriving Show

PlutusTx.makeIsDataIndexed ''BountyAction [('Cancel, 0), ('Update, 1), ('Distribute, 2)]
PlutusTx.makeLift ''BountyAction

data EscrowTypes

instance ValidatorTypes EscrowTypes where
    type DatumType EscrowTypes = BountyEscrowDatum
    type RedeemerType EscrowTypes = BountyAction
