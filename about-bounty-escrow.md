# Gimbalabs Bounty Escrow V0

## Motivation
1. Provide a working example of a bounty contract for further development in (a) PPBL Capstone projects April 2022, and (b) TPBL Technical Group
2. Provide re-usable Plutus contracts and front-end components that A0 Tokenomics Groups can use (at least as a starting place) to distribute tasks that are incentivized with Gimbals.

## Development Agenda
1. Create a testnet branch of https://gitlab.com/jamesdunseith/gimbal-tracker and create transaction builders for (a) creating Bounty UTXOs by pulling funds from Treasury Contract, and (b) allow Bounty Issuer to distribute funds to Contributors who complete bounties
1b. Check on how Front end libs create PKH
2. Re-compile `BountyEscrowDraft` for mainnet
3. Implement mainnet transactions on Gimbal Tracker
4. Further develop BountyTreasury, BountyEscrow, and Front-End in parallel.

## Roles
- Issuer
- Contributor

## Tokens Required
- Some token to be used as bounty incentive (`play` on Testnet, `gimbal` on Mainnet)
- An Auth NFT for each Contributor (`PlutusPBLCourse01` on Testnet, tbd on Mainnet)

## Why two Contracts?
The BountyEscrow Contract only holds funds after a Contributor commits to a Bounty. We need a BountyTreasury Contract to hold a pool of funds to be used in any number of Bounties. The Treasury allows Contributors to make a claim to funds offered for a specific Bounty; that claim creates the UTXO that is locked at the BountyEscrow Contract.

# Getting Started / How to Set Up Your Own Bounty Instance:

## What you will need:

### Cardano Stuff
- A wallet to serve as the Issuer of Bounties
- The CurrencySymbol of an Auth token to be used for Treasury Access and claiming Bounties
- The CurrencySymbol and TokenName of a native asset to be used as Bounty incentive, along with ADA
- A compiled BountyTreasury Plutus script and BountyTreasury Contract address
- A compiled BountyEscrow Plutus script and BountyEscrow Contract address
- The ValidatorHash of the BountyEscrow Contract

### Two Project Repos:
- Plutus Bounty
- Front End

### The BountyEscrow Contract takes the following parameters:
You'll need these in order to compile a specific instance of your Bounty Escrow.

```
data BountyParam = BountyParam
    { bountyTokenPolicyId     :: !CurrencySymbol
    , bountyTokenName         :: !TokenName
    }

```

After you compile the Bounty Escrow contract, you'll need to get its ValidatorHash, which is a BountyTreasury parameter.

### The BountyTreasury Contract takes the following parameters:

You'll need these in order to compile a specific instance of your Treasury
```
data TreasuryParam = TreasuryParam
    { authTokenSymbol   :: !CurrencySymbol
    , bountyContract    :: !ValidatorHash
    , bSymbol           :: !CurrencySymbol
    , bName             :: !TokenName
    }
```


# Testnet Example
- Issuer Wallet Address:
- Issuer Wallet PubKeyHash:
- authTokenSymbol: "3794c001b97da7a47823ad27b29e049985a9a97f8aa6908429180e2c"
- bountyTokenPolicyId / bSymbol: "cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059"
- bountyTokenName / bName: "play"
- bountyContract ValidatorHash: "9a752449b0d3204c4164882ef199a420e3bad74643669eb2420d49c5"
- Treasury Contract Address: "addr_test1wrsdlj8k39g4e3rgsza9lstjqctrlml5mghvywf9m4e288gwxshfq"
- Bounty Contract Address: "addr_test1wzd82fzfkrfjqnzpvjyzauve5ssw8wkhgepkd84jggx5n3gful79d"
